# Fabrique

Based on [fabric.js](http://fabricjs.com/), _Fabrique_ is an A3 poster design tool.

_Fabrique_ is inspired by Tim Rodenbröker's [Lofi Poster Machine](https://github.com/timrodenbroeker/lofi-poster-machine) and Loraine Furter's [Posterator](https://furter.github.io/specimen-hybrides/Presentation/posterator/index.html#).

Designed in the context of the _web to print_ workshop [_L’idéal anarchique_](https://maisondeseditions.fr/ideal) at Bel Ordinaire from 14 to 18 November 2021, it’s been updated to join the [
International Trans★Feminist Digital Depletion Strike](https://constantvzw.org/8m/).

## Source code

The _index.php_ file lists the productions exported in PNG format.

The _create.php_ file exposes the main interface which allows to insert multiline text and images (both user-uploaded and from folders browsed by PHP).

The _save.php_ file allows you to save the image on the server.

## Crédits

The gifs come from [Constant Nextcloud](https://cloud.constantvzw.org/s/q2qom3Fgg2Wjgyp). The taglines from [TITIPI](https://pad.constantvzw.org/p/titipi.stickers).

The fonts available are [Karrik](https://velvetyne.fr/fonts/karrik/) by Jean-Baptiste Morizot and Lucas Le Bihan - [Anthony](https://velvetyne.fr/fonts/anthony/) by Sun Young Oh - [AUTHENTICSans](https://authentic.website/sans.html) by Christina Janus and Desmond Wong - [Bagnard](https://github.com/sebsan/Bagnard) by Sebastien Sanfilippo - [Basteleur](http://velvetyne.fr/fonts/basteleur/) by Keussel - [Compagnon](http://velvetyne. fr/fonts/compagnon/) by Chloé Lozano, Juliette Duhé, Léa Pradine, Sébastien Riollier and Valentin Papon - [Cormorant](https://github.com/CatharsisFonts/Cormorant) by Christian Thalmann - [FivoSansModern](https://www.behance.net/gallery/54442585/FIvo-Sans-Modern-Free-Display-Typeface) by Alex Slobzheninov - [FjallaOne](https://fonts.google.com/specimen/Fjalla+One) by Sorkin Type - [Happy Times](https://velvetyne.fr/fonts/happy-times/) by Lucas Le Bihan - [Morganite](https://www.behance. net/gallery/66748265/Morganite-Free-Typeface-18-Styles) by Rajesh Rajput - [NotCourierSans](http://osp.kitchen/foundry/) by Open Source Publishing - [Orchard](https://github.com/nervousattack/Orchard) by nervousattack - [Panamera](https://noirblancrouge.com/fonts/panamera/) by Black White Red - [PlayfairDisplay](https://fonts.google.com/specimen/Playfair+Display) by Claus Eggers Sørensen - [Poppins](https://fonts.google.com/specimen/Poppins) by Indian Type Foundry - [Rubik](https://fonts. google.com/specimen/Rubik) by Hubert and Fischer and Meir Sadan - [Savate](http://velvetyne.fr/fonts/savate/) by Wech - [SpaceMono](https://www.colophon-foundry.org/custom/spacemono/) by Colophon Foundry - [Terminal grotesque](https://velvetyne.fr/fonts/terminal-grotesque/) by Raphaël Bastide - [VLNL_Tp_Rawkost](https://vetteletters.nl/font/rawkost/) by Vetteletters - [XanhMono](https://fonts.google.com/specimen/Xanh+Mono) by Yellow Type and [Crickx](http://osp.kitchen/foundry/crickx/) by Open Source Publishing.

## Licence

[Fabric.js](http://fabricjs.com/) is available under its own [licence](https://github.com/fabricjs/fabric.js/blob/master/LICENSE).

Fabrique source code is available under the [Unlicence](https://unlicense.org/).

Posters made with the tools are available under the terms of [CC4r * COLLECTIVE CONDITIONS FOR RE-USE](https://constantvzw.org/wefts/cc4r.en.html), a “Copyleft Attitude with a difference” licence that helps to remind us that individual productions are always based on collective achievements.
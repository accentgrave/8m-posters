<!DOCTYPE html>
<html>
<head>
    <title>Fabrique</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
</head>
<body class="create">

  <div id="tools">
    <span class="share">
      <a id="save" href="#" title="Save and share">Share</a>
    </span>
    <span>
      <a href="#" id="add" title="Add text, image, tagline">+</a> 
      <a href="#" id="copyright">©</a>
    </span>
    <span>
      <a id="download" href="#">Download</a>
      <form id="licence">
        <p>By submitting this form, you agree that your poster will be made available under the terms of <a href="https://constantvzw.org/wefts/cc4r.en.html">CC4r * COLLECTIVE CONDITIONS FOR RE-USE</a> licence (Copyleft Attitude with a difference - version 1.0)</p>
      <button type="submit">OK</button>
      <button id="cancelsave" title="Cancel">Cancel</button>
    </form>
    </span>
    <span id="message"></span>
    <div id="imagetools">
      
      <span class="depth">
        <a href="#" data-fabric-action="bringForward">↑</a>
        <a href="#" data-fabric-action="sendBackwards">↓</a>
        <a href="#" data-fabric-action="remove">×</a>
      </span>
      <span class="colors">
        <label style="background-color:#f00"><input type="radio" name="tint" value="#f00"></label>
        <label style="background-color:#ff0"><input type="radio" name="tint" value="#ff0"></label>
        <label style="background-color:#f0f"><input type="radio" name="tint" value="#f0f"></label>
        <label style="background-color:#0f0"><input type="radio" name="tint" value="#0f0"></label>
        <label style="background-color:#00f"><input type="radio" name="tint" value="#00f"></label>
        <label style="background-color:#fff"><input type="radio" name="tint" value="null"></label>
        <label class="rainbow"><input value="#e66465" type="color" name="tint" id="rainbow" value="null"></label>
      </span>
      <span class="type">
        <span class="select">
          <select id="font-family"></select>
        </span>
        <span class="range">
          <input type="range" id="fontsize" name="fontsize" min="8" max="800" value="50" step="1" >
        </span>
        <span class="range">
          <input type="range" id="lineheight" name="lineheight" min="0.2" max="2" value=".8" step=".05" >
        </span>
        <span class="range">
          <input type="range" id="spacing" name="spacing" min="-200" max="600" value="0" step="1" >
        </span>
      </span>
    </div>
  </div>
    
  <div id="addsomething">

    <?php
      function listFolderFiles($dir, $mode){
        $ffs = scandir($dir);
        foreach ($ffs as $ff) {
          if ($ff != '.' && $ff != '..') {
            $file = $dir.'/'.$ff;
            if (stripos($file, '.png') !== false || stripos($file, '.jpg') !== false || stripos($file, '.gif') !== false) {
              echo '<img src="'.$file . '" class="' . $mode . '">';
            }
          }
        }
      }
    ?>

    <button id="canceladd">cancel</button>
    <h2>ADD SOMETHING TO THIS POSTER</h2>

    <details>
      <summary>Free text, from yourself</summary>
      <div id="textform">
        <textarea name="addtext" id="addtext"></textarea>
        <button type="button" id="textblock">OK</button>
      </div>
    </details>
    
    <details>
      <summary>Taglines, from <a href="https://pad.constantvzw.org/p/titipi.stickers">titipi.stickers</a></summary>
      <ul class="taglines">
        <?php $taglines = file('taglines.txt', FILE_IGNORE_NEW_LINES);?>
        <?php foreach ($taglines as $tagline) :?>
          <li class="tagline"><?= $tagline ?></li>
        <?php endforeach ?>
      </ul>
    </details>

    <details>
      <summary>Gifs, from <a href="https://cloud.constantvzw.org/s/q2qom3Fgg2Wjgyp">Constant</a></summary>
      <div class='g'>
        <?php listFolderFiles('_images', 'image');?>
      </div>
    </details>

    <details>
      <summary>Uploads, from everyone</summary>
      <form id="uploader">
        <p>
          <span>
            <label for="yourfile">Your file:</label>
            <input id="yourfile" name="yourfile" id="yourfile" type="file">
          </span>
          <input id="uploaderSubmit" type="submit" value="UPLOAD">          
        </p>
        <p id="disclaimer">Please upload only images (gif, png or jpg) that you are willing to share and for which you own the rights.</p>
      </form>
      <div class='g uploads'>        
        <?php listFolderFiles('_uploads', 'image'); ?>
      </div>
    </details>

  </div>
    
    
  <div id="container">
      <canvas id="c" ></canvas>    
  </div>
  
  <script type="text/javascript" src="js/fabric.js"></script>
  <script type="text/javascript" src="js/fontfaceobserver.js"></script>
  <script type="text/javascript" src="js/resize.js"></script>
  <script type="text/javascript" src="js/script.js"></script>
</body>
</html>
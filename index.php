<!DOCTYPE html>
<html>
<head>
    <title>Fabrique</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

    <div id="tools" style="position: fixed">        
        <a href="create.php" id="create">new poster</a>    
    </div>    
    <p class="mention">
        Posters and images shared as part of the <a href="https://constantvzw.org/8m/">International Trans★Feminist Digital Depletion Strike</a> – <a href="https://gitlab.com/accentgrave/8m-posters">Source code &amp; info</a>
    </p>
    
    <ul id="gallery">
        
            <?php

            $files = glob('_collages/*.png');
            usort($files, function($a, $b) {
                return filemtime($a) < filemtime($b);
            });

            foreach($files as $file){
                if (stripos($file, '.png') !== false ) {
                    echo '<li><a href="'.$file . '" download><img src="'.$file . '"></a></li>';
                }        
            }
           
            ?>
        
    
    </ul>

</body>
</html>
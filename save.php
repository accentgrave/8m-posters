<?php
    // Takes raw data from the request
    $json = file_get_contents('php://input');

    // Converts it into a PHP object
    $data = json_decode($json);

    $img = $data->imgBase64;
    $name = time() . '.' . $extension;
    
    $filename = '_collages/8m-' . sha1($name . uniqid('',true)) . '.png';
    
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $fileData = base64_decode($img);

    if($fileData){
        file_put_contents($filename, $fileData);
        header('Content-type: application/json');
        echo json_encode(['status_code'=>200, "message"=> "All good"]);
    } else {
        header('Content-type: application/json');
        echo json_encode(['status_code'=>500, "message"=>'I tried but it failed… Sorry']);
    }
  
?>
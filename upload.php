<?php
if(isset($_FILES['yourfile'])){
  $file = $_FILES['yourfile']['tmp_name'];

  $verify = getimagesize($file);

  $mime = $verify['mime'];
  $folder = '_uploads/';

  if(($mime != "image/jpeg") && ($mime != "image/gif") && ($mime != "image/png")) {
    echo json_encode([ 'message' => "Error: Upload file type un-recognized. Only .jpg, .gif or .png images allowed" ]);
  } else {
    if(!$width) {
      $extension = pathinfo($_FILES['yourfile']['name'], PATHINFO_EXTENSION);
      $name = time() . '.' . $extension;
    
      if(move_uploaded_file($file, $folder . $name)) {
        // set good permissions for the file
        chmod($folder . $name, 0644);
        // send back info
        $data = array(
          'message' => "Image uploaded",
          'image_source' =>	$folder . $name
        );
        echo json_encode($data);
      } else {
        echo json_encode([ 'message' => "Server Error!" ]);
      } 
    }  
  }

}